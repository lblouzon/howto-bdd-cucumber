# Automation priority: null
# Test case importance: High
# language: en
Feature: Create an account, then try to login with incorrect password

	Scenario: Create an account, then try to login with incorrect password
		Given I am on the AccountCreation page
		When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noël" password "police" email "alice@bob.com" birthDate "1970-01-01" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGdpr "yes" and submit
		And I go to the Home page
		And I sign out
		And I go to the SignIn page
		And I fill SignIn fields with email "alice@bob.com" password "poluce" and submit
		Then The error message "Échec d'authentification" is displayed
