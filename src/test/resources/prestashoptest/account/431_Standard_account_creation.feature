# Automation priority: 42
# Test case importance: High
# language: en
Feature: Standard account creation

	Scenario Outline: Standard account creation
		Given I am on the AccountCreation page
		When I fill AccountCreation fields with gender <gender> firstName <first> lastName <last> password <password> email <mail> birthDate <birth> acceptPartnerOffers <offers> acceptPrivacyPolicy <privacy> acceptNewsletter <news> acceptGdpr <gpdr> and submit
		And I go to the Home page
		And I sign out
		Then I can successfully sign in with email <mail> password <password>
		And The displayed name is <display>
		And My personal information is gender <gender> firstName <first> lastName <last> email <mail> birthDate <birth> acceptPartnerOffers <offers> acceptPrivacyPolicy "no" acceptNewsletter <news> acceptGdpr "no"

		@1
		Examples:
		| birth | display | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "1990-01-02" | "John Doe" | "John" | "M" | "yes" | "Doe" | "jdoe@example.com" | "yes" | "no" | "mypassword" | "yes" |

		@2
		Examples:
		| birth | display | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "1992-01-02" | "Jane Doe" | "Jane" | "F" | "yes" | "Doe" | "jane.doe@oo.com" | "yes" | "yes" | "123&µ" | "yes" |

		@3
		Examples:
		| birth | display | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "1968-02-10" | "Camille Bertillon" | "Camille" | "U" | "yes" | "Bertillon" | "cb@gmail.com" | "no" | "yes" | "£→µ$¤11" | "yes" |

		@4
		Examples:
		| birth | display | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "2022-02-10" | "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" | "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" | "U" | "yes" | "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" | "012345678901234567890123456789012345678901234567890123456789@oo.com" | "yes" | "yes" | "012345678901234567890123456789012345678901234567890123456789" | "yes" |
