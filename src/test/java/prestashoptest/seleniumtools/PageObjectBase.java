package prestashoptest.seleniumtools;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.By.ByPartialLinkText;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Base class for all the page objects
 */
public class PageObjectBase {

    private final String url;
    private final Pattern urlRegex;

    private static Browser browser;
    private static String host;

    private static final Duration TIMEOUT = Duration.ofSeconds(30);

    /**
     * Constructor for pages having a constant URL
     *
     * @param url
     */
    protected PageObjectBase(final String url) {
        Objects.requireNonNull(PageObjectBase.host);
        Objects.requireNonNull(PageObjectBase.browser);
        this.url = url;
        this.urlRegex = null;
    }

    /**
     * Constructor for pages not having a constant URL
     *
     * @param urlRegex
     */
    protected PageObjectBase(final Pattern urlRegex) {
        Objects.requireNonNull(PageObjectBase.host);
        Objects.requireNonNull(PageObjectBase.browser);
        this.url = null;
        this.urlRegex = urlRegex;
    }

    /**
     * Initialise the host (i.e. the base of the URL, e.g. "https://example.com/")
     * This must be done, otherwise the test will crash
     *
     * @param host
     */
    public static void setHost(final String host) {
        PageObjectBase.host = host;
    }

    /**
     * Select the browser to use for running the test
     * This must be done, otherwise the test will crash
     *
     * @param browser
     */
    public static void setBrowser(final Browser browser) {
        PageObjectBase.browser = browser;
    }

    /**
     * Close the browser
     */
    public static void quit() {
        PageObjectBase.browser.quit();
        PageObjectBase.browser = null;
        PageObjectBase.host = null;
    }

    /**
     * Return a screenshot
     *
     * @return
     */
    public static byte[] getScreenshot() {
        return ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * Go to the page
     * The page must have a constant URL, otherwise the test will crash
     */
    public void goTo() {
        if (!hasConstantUrl()) {
            throw new IllegalStateException("The current page object does not support goTo (it has no constant URL)");
        }
        getDriver().get(host + "/" + this.url);
    }

    /**
     * Assert that the page is currently displayed
     */
    public void assertIsCurrent() {
        final String currentUrl = getDriver().getCurrentUrl();
        Assertions.assertTrue(currentUrlIsExpected(currentUrl),
                              "current URL (" + currentUrl
                              + ") does not match the expected one (" + host + "/"
                              + (hasConstantUrl() ? this.url : this.urlRegex) + ")");
    }

    /**
	 * Get all error messages currently displayed
	 * This is specific to Prestashop: we get the texts of the elements having the "alert-danger" class
	 *
	 * @return
	 */
	public static List<String> getErrorMessages() {
	    return SelectBy.CLASS.findElements(getDriver(), "alert-danger")
	                         .stream()
	                         .map(e -> e.getText())
	                         .collect(Collectors.toList());
	}

	/**
     * Wait for the element to be visible
     *
     * @param by
     * @param elementKey
     */
    protected void waitElementIsVisible(final SelectBy by,
                                        final String elementKey) {
        final WebDriverWait wait = new WebDriverWait(getDriver(), TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by.apply(elementKey)));
    }


	/**
     * Wait for the element to be clickable
     *
     * @param by
     * @param elementKey
     */
    protected void waitElementIsClickable(final SelectBy by,
                                          final String elementKey) {
        final WebDriverWait wait = new WebDriverWait(getDriver(), TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(by.apply(elementKey)));
    }

    /**
     * Get the text of the element
     *
     * @param by
     * @param elementKey
     * @return
     */
    protected String getElementText(final SelectBy by,
                                    final String elementKey) {
        return by.findElement(getDriver(), elementKey).getText();
    }

    /**
     * Get an attribute of the element
     *
     * @param by
     * @param elementKey
     * @param attribute
     * @return
     */
    protected String getElementAttribute(final SelectBy by,
                                         final String elementKey,
                                         final String attribute) {
        return by.findElement(getDriver(), elementKey).getAttribute(attribute);
    }

    /**
	 * Get the value of an element
	 *
	 * @param by
	 * @param elementKey
	 * @return
	 */
	protected String getFieldValue(final SelectBy by,
	                               final String elementKey) {
	    return getElementAttribute(by, elementKey, "value");
	}

	/**
     * Fill an element (that element must be a field in a form)
     *
     * @param by
     * @param elementKey
     * @param value
     */
    protected void fillFieldValue(final SelectBy by,
                                  final String elementKey,
                                  final String value) {
        final WebElement element = by.findElement(getDriver(), elementKey);
        element.clear();
        element.sendKeys(value);
    }

    /**
     * Click on an element
     *
     * @param by
     * @param elementKey
     */
    protected void clickElement(final SelectBy by,
                                final String elementKey) {
        by.findElement(getDriver(), elementKey).click();
    }

    /**
     * Hover over an element
     *
     * @param by
     * @param elementKey
     */
    protected void hoverElement(final SelectBy by,
                                final String elementKey) {
        final Actions actions = new Actions(getDriver());
        final WebElement element = by.findElement(getDriver(), elementKey);
        actions.moveToElement(element).perform();
    }

    /**
     * Get the status of an element (that element must be a checkbox)
     *
     * @param by
     * @param elementKey
     * @return
     */
    protected boolean isCheckBoxSelected(final SelectBy by,
                                         final String elementKey) {
        return by.findElement(getDriver(), elementKey).isSelected();
    }

    /**
     * Return the list of element matching the selector
     *
     * @param by
     * @param elementKey
     * @return
     */
    protected List<WebElement> getElements(final SelectBy by,
                                           final String elementKey) {
        return by.findElements(getDriver(), elementKey);
    }

    private static WebDriver getDriver() {
        return PageObjectBase.browser.getDriver();
    }

    private boolean hasConstantUrl() {
        return (this.url != null);
    }

    private boolean currentUrlIsExpected(final String currentUrl) {
	    final String currentUrlWithParams = currentUrl.replaceFirst("[&#].*$", "");
	    if (hasConstantUrl()) {
	        return currentUrlWithParams.equals(host + "/" + this.url);
	    } else {
	        if (!currentUrlWithParams.startsWith(host + "/")) return false;
	        final String pageUrlComponent = currentUrlWithParams.substring(host.length() + 1);
	        final Matcher matcher = this.urlRegex.matcher(pageUrlComponent);
	        return matcher.matches();
	    }
	}

	/**
     * Helper to simplify the definitions of the Selenium selectors
     */
    public enum SelectBy {

        ID(s -> new ById(s)),
        NAME(s -> new ByName(s)),
        XPATH(s -> new ByXPath(s)),
        CLASS(s -> new ByClassName(s)),
        LINKTEXT(s -> new ByLinkText(s)),
        LINKTEXTEXTRACT(s -> new ByPartialLinkText(s));

        private final Function<String, By> selector;

        private SelectBy(final Function<String, By> selector) {
            this.selector = selector;
        }

        private By apply(final String key) {
            return this.selector.apply(key);
        }

        private WebElement findElement(final WebDriver driver,
                                       final String key) {
            return getDriver().findElement(apply(key));
        }

        private List<WebElement> findElements(final WebDriver driver,
                                              final String key) {
            return getDriver().findElements(apply(key));
       }
}

    /**
     * Helper to simplify the use of WebDrivers
     */
    public enum Browser {

        CHROME(() -> new ChromeDriver()),
        FIREFOX(() -> new FirefoxDriver()),
        EDGE(() -> new EdgeDriver());

        private final Supplier<WebDriver> driverBuilder;
        private WebDriver driver;

        private Browser(final Supplier<WebDriver> driverBuilder) {
            this.driverBuilder = driverBuilder;
        }

        private WebDriver getDriver() {
            // we perform a lazy creation of the driver so the tester does not need all possible drivers on his/her machine, s/he just needs the ones s/he uses
            if (this.driver == null) {
                this.driver = driverBuilder.get();
            }
            return this.driver;
        }

        private void quit() {
            this.driver.quit();
            this.driver = null;
        }
    }
}
