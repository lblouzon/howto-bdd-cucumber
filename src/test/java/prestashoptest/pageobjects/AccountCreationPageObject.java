package prestashoptest.pageobjects;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import prestashoptest.datatypes.Gender;
import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Account creation page
 */
public class AccountCreationPageObject extends PageObjectBase {

    public AccountCreationPageObject() {
        super("login?create_account=1");
    }

    /**
     * Fill the form with the specified values and initiate the account creation.
     *
     * @param gender
     * @param firstName
     * @param lastName
     * @param password
     * @param email
     * @param birthDate
     * @param acceptPartnerOffers
     * @param acceptPrivacyPolicy
     * @param acceptNewsletter
     * @param acceptGdpr
     */
    public void fillNewAccountFields(final Gender gender,
                                     final String firstName,
                                     final String lastName,
                                     final String password,
                                     final String email,
                                     final LocalDate birthDate,
                                     final boolean acceptPartnerOffers,
                                     final boolean acceptPrivacyPolicy,
                                     final boolean acceptNewsletter,
                                     final boolean acceptGdpr) {
        fillGender(gender);
        fillFirstName(firstName);
        fillLastName(lastName);
        fillEmail(email);
        fillPassword(password);
        fillBirthDate(birthDate);
        if (acceptPartnerOffers) acceptPartnerOffers();
        if (acceptPrivacyPolicy) acceptPrivacyPolicy();
        if (acceptNewsletter) acceptNewsletter();
        if (acceptGdpr) acceptGdpr();
        submitNewAccountForm();
    }

    /**
     * Fill the gender field
     * If the gender is undefined, the field is untouched.
     *
     * @param gender
     */
    public void fillGender(final Gender gender) {
        if (gender.equals(Gender.UNDEFINED)) {
            return;
        }
        final String id = (gender.equals(Gender.MALE)) ? "field-id_gender-1"
                                                       : "field-id_gender-2";
        clickElement(SelectBy.ID, id);
    }

    /**
     * Fill the first name field
     *
     * @param firstName
     */
    public void fillFirstName(final String firstName) {
        fillFieldValue(SelectBy.ID, "field-firstname", firstName);
    }

    /**
     * Fill the last name field
     *
     * @param lastName
     */
    public void fillLastName(final String lastName) {
        fillFieldValue(SelectBy.ID, "field-lastname", lastName);
    }

    /**
     * Fill the email field
     *
     * @param email
     */
    public void fillEmail(final String email) {
        fillFieldValue(SelectBy.ID, "field-email", email);
    }

    /**
     * Fill the password field
     *
     * @param password
     */
    public void fillPassword(final String password) {
        fillFieldValue(SelectBy.ID, "field-password", password);
    }

    /**
     * Fill the birth date field
     *
     * @param birthDate
     */
    public void fillBirthDate(final LocalDate birthDate) {
        fillFieldValue(SelectBy.ID, "field-birthday", birthDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }

    /**
     * Approve for partner offers
     */
    public void acceptPartnerOffers() {
        clickElement(SelectBy.NAME, "optin");
    }

    /**
     * Approve the customer privacy policy
     */
    public void acceptPrivacyPolicy() {
        clickElement(SelectBy.NAME, "customer_privacy");
    }

    /**
     * Sign up for the newsletter
     */
    public void acceptNewsletter() {
        clickElement(SelectBy.NAME, "newsletter");
    }

    /**
     * Approve the GDPR policy
     */
    public void acceptGdpr() {
        clickElement(SelectBy.NAME, "psgdpr");
    }

    /**
     * Initiate the account creation
     */
    public void submitNewAccountForm() {
        clickElement(SelectBy.XPATH, "//*[@id=\"customer-form\"]/footer/button");
    }
}
